﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowFluffManager : MonoBehaviour {

    public Renderer snow;
    public Vector4 my_pos;

	// Update is called once per frame
	void Update () {
        my_pos = new Vector4(this.transform.position.x, this.transform.position.y, this.transform.position.z, 0.0f);
        snow.material.SetVector("_char_pos", my_pos);
    }
}
